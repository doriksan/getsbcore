"use strict";

$( document ).ready(function() {

    var jsProfileButton = $('.jsProfileButton');

    jsProfileButton.click(function(event) {
        $('html').one('click',function() {
            jsProfileButton.removeClass('is-active');
        });
        jsProfileButton.toggleClass('is-active');
        event.stopPropagation();
   });

    var jsMessageCloseButton = $('.c-mes__close');
    jsMessageCloseButton.click(function() {
        $(this).parent().fadeOut( 1000 );
    });

});
