$( document ).ready(function() {

    var jsExample = $('.jsExample'),
        jsTypingField = $('.jsTypingField'),
        jsTimerEl = $('.jsTimer'),
        jsTypingSpeedEl = $('.jsTypingSpeed'),
        jsErrorsEl = $('.jsErrors'),
        jsVisualSpeedEl = $('.jsVisualSpeedEl'),
        jsVisualSpeedLine = $('.jsVisualSpeedLine'),
        jsErrorItems = $('.jsErrorItems'),
        jsReloadEl = $('.jsReload'),
        jsSpeedTarget = $('.jsSpeedTarget'),
        jsTextSelect = $('.jsTextSelect'),
        speedPercent = 0,
        errorCounter = 0,
        errorDoor = true,
        jsTypingSpeedVal = 0,
        jsCharactersNumberVal = 0,
        timerCounter = 0,
        timerRun = false,
        jsTypingFieldVal,
        jsSubstrExample,
        jsExampleVal;

    var speedTarget = jsSpeedTarget.text();

    setInterval(function(){
        if(timerRun == true) {
            jsTimerEl.html(timerCounter);
            timerCounter++;
            setTypingSpeed(jsCharactersNumberVal, timerCounter);
        }
    }, 1000);

    jsReloadEl.on('click', function() {
        location.reload();
    });

    jsTextSelect.on('input', function() {
        $('#text_form').submit();
    });

    jsTypingField.on('input', function() {

        jsExampleVal = jsExample.text();
        jsTypingFieldVal = jsTypingField.val();
        jsCharactersNumberVal = jsTypingFieldVal.length;
        jsSubstrExample = jsExampleVal.substr(0, jsCharactersNumberVal);

        if(checkRight(jsTypingFieldVal, jsSubstrExample)) {
            setNormal(jsTypingField);
            if(errorDoor == false) {
                errorDoor = true;
            }
        } else {
            setError(jsTypingField);
            if(errorDoor == true) {
                errorCounter++;
                //putErrorCounter(errorCounter, jsErrorsEl);
                errorDoor = false;

                var jsErrorCounter = 0;
                var jsErrorItemCounter = jsErrorCounter + 1;
                while(jsErrorCounter < errorCounter) {
                    jsErrorItems.find('.b-error__item:nth-child(' + jsErrorItemCounter + ')').addClass('is-active');
                    jsErrorItemCounter++;
                    jsErrorCounter++;
                }
                if(errorCounter == 3) {
                    location.reload();
                }

            }
        }
        if(jsCharactersNumberVal == 1) {
            timerRun = true;
        } else if (jsCharactersNumberVal == 0) {
            resetTimer();
            jsTypingSpeedEl.html(0);
            jsVisualSpeedEl.html(0);
            errorCounter = 0;
            //putErrorCounter(errorCounter, jsErrorsEl);
            resetPercentToLine(jsVisualSpeedLine);
            jsErrorItems.find('.b-error__item').removeClass('is-active');
        }
        if(jsCharactersNumberVal > 2) {
            setTypingSpeed(jsCharactersNumberVal, timerCounter);
        }

        if(jsExampleVal.length == jsCharactersNumberVal && errorDoor != false) {
            jsTypingField.attr('disabled','disabled').css('opacity', '0.7');
            var jsResForm = $('#textarea');
            jsResForm.find('input[name="speed"]').attr('value', getTypingSpeed(jsCharactersNumberVal, timerCounter));
            jsResForm.find('input[name="errors"]').attr('value', errorCounter);
            jsResForm.submit();

        }

    });

    function checkRight(typingVal, exampleVal) {
        return (typingVal == exampleVal) ? true : false;
    }
    function setError(el) {
        el.css('border-color', 'red').css('color', 'red');
    }
    function setNormal(el) {
        el.css('border-color', 'black').css('color', 'black');
    }
    function setTypingSpeed(characters, time) {
        if(time != 0) {
            jsTypingSpeedVal = (60 * characters / time);
            jsTypingSpeedVal = Math.round(jsTypingSpeedVal);
            jsTypingSpeedEl.html(jsTypingSpeedVal);
            jsVisualSpeedEl.html(jsTypingSpeedVal);
            setPercentToLine(jsVisualSpeedLine, getSpeedPercent(jsTypingSpeedVal, speedTarget));
        }
    }
    function getTypingSpeed(characters, time) {
        if(time != 0) {
            jsTypingSpeedVal = (60 * characters / time);
            jsTypingSpeedVal = Math.round(jsTypingSpeedVal);
            return jsTypingSpeedVal;
        }
    }
    function resetTimer() {
        timerRun = false;
        timerCounter = 0;
        jsTimerEl.html(timerCounter);
    }
    function putErrorCounter(counter, el) {
        el.html(counter);
    }
    function getSpeedPercent(typingSpeed, target) {
        speedPercent = typingSpeed * 100 / target;
        speedPercent = Math.round(speedPercent);
        return speedPercent;
    }
    function setPercentToLine(line, percent) {
        line.css('width', percent + '%');
    }
    function resetPercentToLine(line) {
        line.css('width', '1%');
    }

});
